#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <string>

class Shape {

public:
  virtual ~Shape() = default;
  virtual double Solve(const std::string&) const = 0;
};

#endif
