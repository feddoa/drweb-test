#include <iostream>

#include "shape.hpp"

std::unique_ptr<Shape> Figure(const std::string&);

int main() {

  {
    auto s = Figure("circle")->Solve("5");
    std::cout << s << std::endl;
  }

  {
    auto s = Figure("rect")->Solve("10 8");
    std::cout << s << std::endl;
  }

  {
    auto s = Figure("triangle")->Solve("6 6");
    std::cout << s << std::endl;
  }

  try {
    auto s = Figure("not_found")->Solve("50");
    std::cout << s << std::endl;
  }
  catch (std::exception& e) {
    std::cout << e.what() << std::endl;
  }

  return 0;
}
