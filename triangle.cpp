#include "triangle.hpp"

double Triangle::Solve(const std::string& args) const {
  auto pos = args.find(" ");
  return 0.5 * std::stod(args.substr(0, pos)) * std::stod(args.substr(pos + 1));
}