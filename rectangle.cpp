#include "rectangle.hpp"

double Rectangle::Solve(const std::string& args) const {
  auto pos = args.find(" ");
  return std::stod(args.substr(0, pos)) * std::stod(args.substr(pos + 1));
}