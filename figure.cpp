#include <string>
#include <memory>
#include <stdexcept>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

std::unique_ptr<Shape> Figure(const std::string& figure) {
  if (figure == "circle") {
    return std::make_unique<Circle>();
  }

  if (figure == "rect") {
    return std::make_unique<Rectangle>();
  }

  if (figure == "triangle") {
    return std::make_unique<Triangle>();
  }

  throw std::invalid_argument("Invalid figure name. Expect circle, rect or triangle.");
}
