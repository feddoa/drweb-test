#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape {

public:
  double Solve(const std::string&) const override;
};

#endif
