#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape {

public:
  double Solve(const std::string&) const override;
};

#endif