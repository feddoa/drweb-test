#define _USE_MATH_DEFINES
#include <cmath>
#include "circle.hpp"

double Circle::Solve(const std::string &args) const {
  return std::pow(std::stod(args), 2) * M_PI;
}
